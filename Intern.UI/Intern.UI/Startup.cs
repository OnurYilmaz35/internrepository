﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Intern.UI.Startup))]
namespace Intern.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
